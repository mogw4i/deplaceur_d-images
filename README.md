## Déplaçeur d'images

### Auteur
Diane (Mogw4i)

## Languages
    - HTML
    - CSS
    - JavaScript

# Fonctionnement
Site web HTML/CSS où l'utilisateur, à l'aide de boutons, peut déplacer une image spécifique (ici : un burger).
La partie JS effectue le déplacement des images, à l'aide d'objets image.

# Crédits
Images : @oudartsy (Instagram)
(Il s'agit de moi-même lol)
